﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1
{
    public sealed class FStore
    {
        private static FStore instance;
        private List<BookEntity> books;

        private FStore()
        {
            books = new List<BookEntity> {
                new BookEntity(){
                    Id = 1, Title="Fundamentals of Wavelets",
                    Authors= new String[] {"Goswami","Jaideva" },
                    Categories= new String[] {"technology"},
                    PageCount=228,
                    Description="Technology book about how wavelets work and how to repair them in the needed case.",
                    ImgUrl="https://covers.openlibrary.org/b/id/5073863-M.jpg",
                    Rate=3,
                    RateCount=2
                },
                new BookEntity(){
                    Id = 2, Title="The Devil's Eden",
                    Authors= new String[] {"Elizabeth Power" },
                    Categories= new String[] {"fiction", "general"},
                    PageCount=188,
                    Description="Coralie Rhodes - working under the name of Lee Roman - had expected her assignment in Bermuda to be reasonably straightforward. But that was before she met the tycoon she was to interview. And, more importantly, it was before she met his nephew - Jordan Colyer. Eight years ago Jordan had made his hatred and disgust of her quite clear, and although he didn't seem to recognise her now it would only be a matter of time. And time was what she didn't have.",
                    ImgUrl="https://covers.openlibrary.org/b/id/5073863-M.jpg",
                    Rate=2.5,
                    RateCount=185
                },
                new BookEntity(){
                    Id = 3, Title="Coming Home",
                    Authors= new String[] {"Coming Home" },
                    Categories= new String[] {"fiction", "horses", "juvenile"},
                    PageCount=140,
                    Description="Amy has always had her mother to help with Heartland, a place where the scars of the past can be healed, a place where frightened and abused horses can learn to trust again, but recently she passed away heading to help a stallion who was locked in a barn during a storm. The fateful car crash has made it so that everything at heartland has gone hay wire, and Amy has to suddenly take on her mother's responsibility, with the help of Ty, the stable hand. The herb remedies are a lot to memorize, and the other training methods Amy's mother used were also difficult to learn, and as the barn gets swamped in work, Amy has to decide whether or not to continue with Heartland. Her father is not around due to an accident involving his horse, Pegasus, when Amy was 2 or 3 years old. ",
                    ImgUrl="https://covers.openlibrary.org/b/id/275967-M.jpg",
                    Rate=4,
                    RateCount=1
                },
                new BookEntity(){
                    Id = 4, Title="20,000 leagues under the sea",
                    Authors= new String[] {"Judith Conaway" },
                    Categories= new String[] {"science fiction", "fiction", "sea stories"},
                    PageCount=228,
                    Description="An adaptation of the nineteenth-century science fiction tale of an electric submarine, its eccentric captain, and the undersea world, which anticipated many of the scientific achievements of the twentieth century. .",
                    ImgUrl="https://covers.openlibrary.org/b/id/6640382-M.jpg",
                    Rate=5,
                    RateCount=19
                },
                new BookEntity(){
                    Id = 5, Title="The prime minister",
                    Authors= new String[] {"Anthony Trollope" },
                    Categories= new String[] {"technology"},
                    PageCount=128,
                    Description="The first plot records the clash between the Duke of Omnium, now prime minister of a coalition government, and his meddlesome wife, Lady Glencora. Whose well-intended maneuverings result in embarrassment for the Duke's office. The second plot tells the scheming of Ferdinand Lopez, a ruthless social climber who wins the support of Lady Glencora--but not her husband--for an election campaign. The straightforwardness of the structure allows Trollope to plan the vivid scenes of intensity and character conflict. The incidents of The Prime Minister are planned with a brilliant astuteness. Anyone who is interested in Victorian history and politics will find the novel enchanting. ",
                    ImgUrl="https://covers.openlibrary.org/b/id/9905984-M.jpg",
                    Rate=5,
                    RateCount=145
                },
                new BookEntity(){
                    Id = 6, Title="The noble jilt, a comedy",
                    Authors= new String[] {"Anthony Trollope" },
                    Categories= new String[] {"fiction", "classic", "general"},
                    PageCount=122,
                    Description="Alice Vavasor cannot decide whether to marry her ambitious but violent cousin George or the upright and gentlemanly John Grey - and finds herself accepting and rejecting each of them in turn. Increasingly confused about her own feelings and unable to forgive herself for such vacillation, her situation is contrasted with that of her friend Lady Glencora - forced to marry the rising politician Plantagenet Palliser in order to prevent the worthless Burgo Fitzgerald from wasting her vast fortune. In asking his readers to pardon Alice for her transgression of the Victorian moral code, Trollope created a telling and wide-ranging account of the social world of his day. ",
                    ImgUrl="https://covers.openlibrary.org/b/id/5073863-M.jpg",
                    Rate=3.3,
                    RateCount=14
                },
                new BookEntity(){
                    Id = 7, Title="The crisis.",
                    Authors= new String[] {"Winston Churchill" },
                    Categories= new String[] {"classic"},
                    PageCount=177,
                    Description="",
                    ImgUrl="https://covers.openlibrary.org/b/id/8246142-M.jpg",
                    Rate=4,
                    RateCount=15
                },
                new BookEntity(){
                    Id = 8, Title="Aunt Maria",
                    Authors= new String[] {"Diana Wynne Jones" },
                    Categories= new String[] {"magic", "fiction"},
                    PageCount=214,
                    Description="",
                    ImgUrl="https://covers.openlibrary.org/b/id/430662-M.jpg",
                    Rate=0,
                    RateCount=5
                },
                new BookEntity(){
                    Id = 9, Title="Development in adulthood",
                    Authors= new String[] {"Barbara Hansen Lemme" },
                    Categories= new String[] {"psychology"},
                    PageCount=544,
                    Description=".",
                    ImgUrl="https://covers.openlibrary.org/b/id/1144777-M.jpg",
                    Rate=3,
                    RateCount=12
                },
                new BookEntity(){
                    Id = 10, Title="Haunted",
                    Authors= new String[] {"Heather Graham" },
                    Categories= new String[] {"paranormal", "fiction", "romance"},
                    PageCount=384,
                    Description="Matt Stone doesn't believe in ghosts. But there are those who are convinced his home, a historic Virginia estate that dates back to the Revolutionary War, is haunted. Pressured to get at the truth about some strange happenings at Melody House, he agrees to let Harrison Investigations explore the house.But he isn't ready for beautiful, intriguing Darcy Tremayne. As a paranormal investigator, Darcy has learned to believe in the unbelievable. And she's given Matt fair warning: sometimes people don't like the skeletons she finds. She never dreamed that warning would apply to herself. For she's about to discover that Melody House holds much more than a simple mystery from the distant past. What it holds is a very real and lethal danger, one that will cast her into a struggle against the worlds of both the living and the dead.",
                    ImgUrl="https://covers.openlibrary.org/b/id/771524-M.jpg",
                    Rate=4,
                    RateCount=6
                },
                new BookEntity(){
                    Id = 11, Title="Esperanza",
                    Authors= new String[] {"Danielle Steel" },
                    Categories= new String[] {"hope"},
                    PageCount=240,
                    Description="NO description yet.",
                    ImgUrl="https://covers.openlibrary.org/b/id/2304121-M.jpg",
                    Rate=3,
                    RateCount=2
                },
                new BookEntity(){
                    Id = 12, Title="Picasso",
                    Authors= new String[] { "Pablo Picasso", "Jean-Louis Andral", "Pierre Daix" },
                    Categories= new String[] {"art", "ceramics", "sculpture", "drawing"},
                    PageCount=240,
                    Description="Everyone wants to understand art.",
                    ImgUrl="https://covers.openlibrary.org/b/id/2238306-M.jpg",
                    Rate=3.74,
                    RateCount=2
                },
                new BookEntity(){
                    Id = 13, Title="EL JUEGO DE GALLOS EN NUEVA ESPANA",
                    Authors= new String[] { "MARIA JUSTINA SARABIA VIEJO" },
                    Categories= new String[] {"general", "spanish"},
                    PageCount=149,
                    Description=" ASIN : B01NH2WHHT Editorial : SEVILLE: G.E.H.A. (1 Enero 1972).",
                    ImgUrl="https://images-na.ssl-images-amazon.com/images/I/41HbliTym0L._SX350_BO1,204,203,200_.jpg",
                    Rate=0,
                    RateCount=0
                },
                new BookEntity(){
                    Id = 14, Title="Siempre Joven",
                    Authors= new String[] { " Dr. Alexander Leaf" },
                    Categories= new String[] {"general", "classic" },
                    PageCount=149,
                    Description="Alexander Leaf was a versatile physician and research scientist who was an early advocate of diet and exercise to prevent heart disease, who traveled the world to make important discoveries about increasing human longevity and to help scientifically establish the dangers global warming poses to the human species..",
                    ImgUrl="https://images-na.ssl-images-amazon.com/images/I/51xeqRO301L._SX334_BO1,204,203,200_.jpg",
                    Rate=4,
                    RateCount=16
                }
            };
        }

        static FStore ()
        {
            instance = new FStore();
        }

        public static FStore Instance
        {
            get { return instance; }
        }

        public List<BookEntity> getBooks()
        {
            return books;
        }

        public int addBook(BookEntity newBook)
        {
            books.Add(new BookEntity()
            {
                Id = books.Count + 1,
                Title = newBook.Title,
                Authors = newBook.Authors,
                Categories = newBook.Categories,
                Description = newBook.Description,
                ImgUrl = newBook.ImgUrl,
                PageCount = newBook.PageCount,
                Rate = newBook.Rate
            });
            return books.Count;
        }

        public bool deleteBookById(int id)
        {
            var bookDelete = books.Find(b => b.Id == id);
            if (bookDelete == null)
            {
                return false;
            }
            books.Remove(bookDelete);
            return true;
        }
    }
}
