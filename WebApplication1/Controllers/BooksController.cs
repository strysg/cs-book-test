﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {

        // public static List<BookEntity> allBooks { get; set; }

        [HttpGet]
        public async Task<ActionResult<List<BookEntity>>> Get()
        {
            //var bookList = await GetBooksList();
            FStore fstore = FStore.Instance;
            if (fstore.getBooks().Count < 0)
            {
                return NotFound();
            }
            return fstore.getBooks();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BookEntity>> GetById(int id) {
            FStore fstore = FStore.Instance;
            try
            {
                var book = fstore.getBooks().First(b => b.Id == id);
                return book;
            } catch (Exception e)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult<List<BookEntity>>> Post(BookEntity bookEntity)
        {
            FStore fstore = FStore.Instance;
            fstore.addBook(new BookEntity()
            //allBooks.Add(new BookEntity()
            {
                Id = fstore.getBooks().Count + 1,
                Title = bookEntity.Title,
                Authors = bookEntity.Authors,
                Categories = bookEntity.Categories,
                Description = bookEntity.Description,
                ImgUrl = bookEntity.ImgUrl,
                PageCount = bookEntity.PageCount,
                Rate = bookEntity.Rate
            }); ;
            return fstore.getBooks();
            //return new ActionResult<List<BookEntity>>(allBooks);
        }

        [HttpPut]
        public async Task<ActionResult<List<BookEntity>>> Put(BookEntity bookEntity)
        {
            FStore fstore = FStore.Instance;
            var bookUpdate = fstore.getBooks().Find(u => u.Id == bookEntity.Id);
            if (bookUpdate == null)
                 return NotFound();

            fstore.getBooks().First(u => u.Id == bookUpdate.Id).Title = bookEntity.Title;
            fstore.getBooks().First(u => u.Id == bookUpdate.Id).Rate = bookEntity.Rate;
            fstore.getBooks().First(u => u.Id == bookUpdate.Id).Description = bookEntity.Description;
            fstore.getBooks().First(u => u.Id == bookUpdate.Id).Categories = bookEntity.Categories;
            fstore.getBooks().First(u => u.Id == bookUpdate.Id).Authors = bookEntity.Authors;
            fstore.getBooks().First(u => u.Id == bookUpdate.Id).ImgUrl = bookEntity.ImgUrl;
            fstore.getBooks().First(u => u.Id == bookUpdate.Id).PageCount = bookEntity.PageCount;

            return fstore.getBooks();
        }
 
        [HttpDelete("{id}")]
        public async Task<ActionResult<List<BookEntity>>> Delete(int id)
        {
            FStore fstore = FStore.Instance;
            if (fstore.deleteBookById(id) == false)
            {
                return NotFound();
            }
            return fstore.getBooks();
        }

        [HttpPost("rate/{id}/{rate}")]
        public async Task<ActionResult<BookEntity>> Rate(int id, int rate)
        {
            FStore fstore = FStore.Instance;
            try
            {
                var book = fstore.getBooks().First(b => b.Id == id);
                // calculating new ratio
                Double newRate = (book.Rate * book.RateCount + rate) / (book.RateCount + 1);

                fstore.getBooks().First(b => b.Id == id).Rate = newRate;
                fstore.getBooks().First(b => b.Id == id).RateCount = book.RateCount + 1;
                return fstore.getBooks().First(b => b.Id == id);
            }
            catch (Exception e)
            {
                return NotFound();
            }
        }
    }
}
