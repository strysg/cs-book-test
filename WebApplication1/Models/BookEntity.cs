﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class BookEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public String[] Authors { get; set; }
        public String[] Categories { get; set; }
        public Double Rate { get; set; }
        public int PageCount { get; set; }
        public int RateCount { get; set; }

    }
}
